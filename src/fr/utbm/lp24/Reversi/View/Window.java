package fr.utbm.lp24.Reversi.View;

import javax.swing.JFrame;

public class Window extends JFrame{

    public Window(){
        this.setTitle("Reversi");
        this.setSize(400, 400);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

}
